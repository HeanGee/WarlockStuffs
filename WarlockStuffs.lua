SLASH_SOULSTONE1 = "/ss"
SLASH_SOULSTONE2 = "/soulstone"
SLASH_COE1 = "/coe"
SLASH_COT1 = "/cot"
SLASH_HS1 = "/hs"
function truncateUntilInteger(a,b)
    return (a - a % b) / b
end
function secondsToDate(seconds)
	if seconds >= 3600 then
		local hour = truncateUntilInteger(seconds, 3600)
		local full_minutes = ( (seconds / 3600) - hour ) * 60
		local minutes = truncateUntilInteger(full_minutes, 60)
		local full_seconds = (full_minutes - minutes) * 60
		return (hour .. ":" .. string.format( "%02d", minutes ) .. ":" .. string.format( "%02d", full_seconds ))
	elseif seconds >= 60 then
		local full_minutes = seconds / 60
		local minutes = truncateUntilInteger(seconds, 60)
		local full_seconds = ( full_minutes - minutes ) * 60
		return ( string.format( "%02d", minutes ) .. ":" .. string.format( "%02d", full_seconds ) )
	else
		return string.format( "%02d", seconds )
	end
end
SlashCmdList["SOULSTONE"] = function(self, txt)

	if (UnitAffectingCombat("player")~=1) then  -- Don't run macro when not in combat.
		return
	end

	local soulstoneID = 36895
	local ssUsedTime, ssCD, _ = GetItemCooldown(soulstoneID)
	local _, soulstoneLink = GetItemInfo(soulstoneID)
	local ssinrange = IsItemInRange(soulstoneID, "target")
	local targetName = GetUnitName("target")
	local now = GetTime()
	local ssCDRemain = ssCD - (now - ssUsedTime)
	
	--SendChatMessage("used: " .. ssUsedTime,"RAID")
	--SendChatMessage("now: " .. now,"RAID")
	--SendChatMessage("time elapsed: " .. ssCDRemain,"RAID")
	--SendChatMessage("duration: " .. ssCD,"RAID")
	
	if not (ssUsedTime == 0) then
		SendChatMessage("{rt1} " .. soulstoneLink .. " will be ready within " .. secondsToDate(ssCDRemain) .. ". {rt1}","RAID")
	else
		if targetName == nil or (UnitCanAttack("player","target")) then
			SendChatMessage("{rt1} " .. GetUnitName("player") .. " is casting " .. soulstoneLink .. " on himself! {rt1}","RAID")
		elseif ssinrange == 0 then
		   SendChatMessage("{rt1} %t is too away for " .. soulstoneLink .. "! {rt1}","RAID")
		elseif ssinrange == 1 then
			SendChatMessage("{rt1} Casting "..soulstoneLink.." on %t! {rt1}","RAID")
		end
	end
end
SlashCmdList["COE"] = function(self, txt)
local curseName = "Curse of the Elements"
	if IsCurrentSpell(curseName) == 1 then
	   if GetUnitName("target") then
		  SendChatMessage("{rt1} " .. GetSpellLink(curseName) .. " on %t! {rt1}","RAID")
	   elseif GetUnitName("mouseover") then
		  SendChatMessage("{rt1} " .. GetSpellLink(curseName) .. " on " .. GetUnitName("mouseover") .. "! {rt1}","RAID")
	   end
	end
end
SlashCmdList["COT"] = function(self, txt)
	--print('al menos entro')
	local spellID = "Curse of Tongues"
	local spellUsed, spellCD, _ = GetSpellCooldown(spellID)
	local now = GetTime()
	local timeRemain = spellCD - (now - spellUsed)
	
	if timeRemain > 1.5 then
		SendChatMessage("{rt1} " .. GetSpellLink(spellID) .. " will be ready within " .. secondsToDate(timeRemain) .. ". {rt1}","RAID")
	else
		SendChatMessage("{rt1} " .. GetUnitName("player") .. " used " .. GetSpellLink(spellID) .. "!","RAID")
	end
end
SlashCmdList["HS"] = function(self, txt)
	local itemID = 36894
	local hsUsedTime, hsCD, _ = GetItemCooldown(itemID)
	local _, hsLink = GetItemInfo(itemID)
	local selfName = GetUnitName("player")
	local now = GetTime()
	local hsCDRemain = hsCD - (now - hsUsedTime)
	local isCurrentItem = IsCurrentItem(itemID)
	local name = "Create Healthstone"

	if IsCurrentSpell(name) and GetItemCount(itemID, false, true) == 1 then
		SendChatMessage("{rt1} YOU can't create more " .. hsLink .. " because it has full charge " .. selfName .. "!" , "RAID")
		return
	elseif IsCurrentSpell(name) and GetItemCount(itemID, false, true) < 1 then
		SendChatMessage("{rt1} " .. selfName .. " is creating " .. hsLink .. "! {rt1}" , "RAID")
		return
	end
	
	if not (hsUsedTime == 0) then
		SendChatMessage("{rt1} " .. hsLink .. " will be ready within " .. secondsToDate(hsCDRemain) .. ". {rt1}","RAID")
	elseif (isCurrentItem) then
		SendChatMessage("{rt1} " .. selfName .. " used " .. hsLink .. " {rt1}", "RAID")
	end
end